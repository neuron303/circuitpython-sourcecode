####
# Late 2022
# Author overflo 
# Part of CANarchy
# Filename: slcand.py
# Purpose: implements the serial can support on linux (slcand)
# License Details found @ /LICENSE file in this repository


import board
import busio
import supervisor
import time


class slcan:

        canarchy = None
        can = None
        debuguart = None
        fifo =''
        active = False

        def __init__(self,canarchy):
                print("slcan.__init__()")
                self.canarchy=canarchy
                self.can=canarchy.can
                
                # DEBUG UART IS OPENED ON PIS 39/40
                # PIN 40 IS THE ONLY RELEVANT ONE AS WE OUTPUT DEBUG INFORMATION THERE
                # SO FAR WE DO NOT READ FROM THE DEBUG UART
                # THE BAUDRATE IS FIXED AT 9600 BAUD !

                self.debuguart=busio.UART(board.IO39,board.IO40)
                self.debuguart.write(b"DEBUG UP!\r\n")
 

        def serial_in(self):
                if supervisor.runtime.serial_bytes_available:
                        # v = input()
                        # print("before strip: ", v)
                        value = input().strip()
                        # Sometimes Windows sends an extra (or missing) newline - ignore them
                        if value == "":
                                return


                        

                        self.debuguart.write(b"RAW: ->{}<-\r\n".format(value))


                        if(value[0] == 'T'):
                                # Transmit an extended (29bit) CAN frame.

                                #id=value[1-8]
                                #size=value[9]
                                #payload=value[10:10+(2*int(size))]
                                #self.debuguart.write(b"ID: {} [{}] = {}\r\n".format(value,size,payload))

                                temp = list(value[1:])
                                temp[8] = '#'
                                packet = "".join(temp)

                                self.debuguart.write(b"SENDING: [T] {}\r\n".format(packet))
                                self.can.send_block(packet)
                                #TODO check if can works or dont work
                                self.ack()
       


                        if(value[0] == 't'):
                                # Transmit a standard (11bit) CAN frame.
                                temp = list(value[1:])
                                temp[3] = '#'
                                packet = "".join(temp)

                                self.debuguart.write(b"SENDING: [t]  {}\r\n".format(packet))
                                self.can.send_block(packet)
                                self.ack()
 


                        if(value[0] == 'C'):
                                # Close channel command, ignore request
                                self.canarchy.blinkled(self.canarchy,3)
                                # enable normal serial communication again
                                self.canarchy.serialcom.start()
                                self.active = False
                                self.ack()
                                self.canarchy.display.set_active_flag(2,False)


                        if(value[0] == 'S'):
                                # set baudrate on canbus
                                speed = int(value[1])

                                '''
                                S0 10Kbit
                                S1 20Kbit
                                S2 50Kbit
                                S3 100Kbit
                                S4 125Kbit
                                S5 250Kbit
                                S6 500Kbit
                                S7 800Kbit
                                S8 1Mbit
                                '''
     
                                if(speed ==0): self.can.set_baudrate(10000)
                                if(speed ==1): self.can.set_baudrate(20000)
                                if(speed ==2): self.can.set_baudrate(50000)
                                if(speed ==3): self.can.set_baudrate(100000)
                                if(speed ==4): self.can.set_baudrate(125000)
                                if(speed ==5): self.can.set_baudrate(250000)
                                if(speed ==6): self.can.set_baudrate(500000)       
                                if(speed ==7): self.can.set_baudrate(800000)
                                if(speed ==8): self.can.set_baudrate(1000000)        
                                self.ack()

                        if(value[0] == 'F'):
                                # Read Status Flags.  // Not implemented yet, return everything is good.
                                print('F'+str(0x00),end='')
                                self.ack()

                        if(value[0] == 'O'):
                                self.start()



                        if(value[0] == 's'):
                                # Setup with BTR0/BTR1 CAN bit-rates where xx and yy is a hex value. 
                                # not implemented
                                self.fail()


                        if(value[0] == 'r'):
                                # Transmit a standard RTR (11bit) CAN frame.
                                # not implemented
                                self.fail()

                        if(value[0] == 'R'):
                                # Transmit an extended RTR (29bit) CAN frame.
                                # not implemented
                                self.fail()

                        if(value[0] == 'M'):
                                # Sets Acceptance Code Register (ACn Register of SJA1000).
                                # not implemented
                                self.fail()

                        if(value[0] == 'm'):
                                # Sets Acceptance Mask Register (AMn Register of SJA1000).
                                # not implemented
                                self.fail()

                        if(value[0] == 'V'):
                                # Get Version number of both CANUSB hardware and software
                                # not implemented
                                print('F1010',end='')
                                self.ack()



                        if(value[0] == 'N'):
                                # Get Serial number of the CANUSB.
                                print('N1337',end='')
                                self.ack()

                        if(value[0] == 'Z'):
                                # Sets Time Stamp ON/OFF for received frames only.
                                # not implemented
                                self.fail()

        
        def serial_out(self):
                if(len(self.can.outbuffer)):
                        #self.debuguart.write(bytearray(self.can.outbuffer))
                        #self.debuguart.write(b"\r\nsending something\r\n")
                        for packet in self.can.outbuffer:
                                self.ack()
                                id,payload=str(packet).split("#") 

                                if(int(len(str(id)))>3):
                                        send="T"+id+str(int(len(payload)/2))+payload
                                else:
                                        send="t"+id+str(int(len(payload)/2))+payload

                                #send="T112233448AABBCCDDEEFF1122"
                                print(send,end='')
                                self.ack()

                                self.debuguart.write(b"sending {}\r\n".format(send))

                                
                        self.can.outbuffer = []


        def ack(self):
                self.debuguart.write(b"[ACK]\r\n")
                print(chr(13),end='')


        def fail(self):
                self.debuguart.write(b"[FAIL]\r\n")
                print(chr(7),end='')



        def start(self):
                self.canarchy.blinkled(self.canarchy,5)
                # turn off "normal"serialcom
                self.canarchy.serialcom.stop()
                self.active = True
                self.canarchy.display.set_active_flag(2,True)
                # Open channel command, ignore request
                for i in range(3):
                        self.ack()


        def work(self):
                if not self.active:
                        return
                
                self.serial_in()
                if (self.active):
                        self.serial_out()
