from binascii import hexlify
from digitalio import DigitalInOut, Direction
import board
import time

from canarchy import canarchy as c

# return the canarchy SINGLETON
canarchy = c.CANarchy()


# Button handlers
def button_callback_onTouch(label):
    if label == "button3":
        for i in range(3):
            canarchy.send("00001337#2342")

    if label == "button2":
        canarchy.toggle_bus_en()
        print("toggled BUS_EN pin")

    if label == "button1":
        canarchy.send("1AA#DEADF000")

    print("button_callback_onClick called for: " + label)





# Hall effect sensor

def hall_callback_onChange(state):
    print("hall_callback_onChange() called with:  ", state)
    if state == False:
        print("Strong magnetic field detected! We might enable somethig here.")








# A periodic callback called as often as possible
last_callback = 0


def periodic_callback():
    global last_callback
    now = time.monotonic()
    if last_callback == 0:
        last_callback = now

    # do something once per second
    if (
        now - last_callback > 1
    ):  # 1 defines how many seconds to wait, the value is a float and alows for sub second resolution.
        last_callback = now
        # print("DOING THIS EVERY SECOND")


# called on CAN receive or send, implement logging here or something else.
def can_callback_onReceive(message):
    # print("can_callback_onReceive")
    data = message.data
    id_str = str(hex(message.id))[2:]
    # packet = "%s#%s" % (f'{int(id_str):08}',hexlify(message.data).decode("utf-8").upper())
    # print("CAN < " + packet)
    # can.dl.log(packet)


def can_callback_onSend(message):
    # print("can_callback_onSend")
    data = message.data
    id_str = str(hex(message.id))[2:]
    # print("CAN > %s#%s"%(f'{id_str:0>8}',hexlify(message.data).decode().upper()))





# not implemented yet
# # SD handlers

# def sd_callback_onClose():
#     print("sd_callback_onClose")

# def sd_callback_onInsert():
#     print("sd_callback_onInsert")

# def sd_callback_onEject():
#     print("sd_callback_onEject")
