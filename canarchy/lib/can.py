####
# Late 2022
# Author overflo 
# Part of CANarchy
# Filename: can.py
# Purpose: canbus support
# License Details found @ /LICENSE file in this repository




import struct
import board
import canio
import digitalio
from binascii import hexlify, unhexlify
import time


try:
        from app import can_callback_onReceive
except:
        print("ERROR: can_callback_onReceive not defined in app.py")

try:
        from app import can_callback_onSend
except:
        print("ERROR: can_callback_onSend not defined in app.py")



class CANbus:

        can = None
        listener= None

        rx=None
        tx=None
        baudrate=None
        
        matchlist=[]

        logger = None

        outbuffer=[]

        display_traffic = False


        canarchy=None

        def __init__(self,RX,TX,BAUDRATE,canarchy):
                print("CANbus.__init__()")
                self.rx=RX
                self.tx=TX

                self.can = canio.CAN(rx=RX, tx=TX, baudrate=BAUDRATE, auto_restart=True)
                self.start_listener()

                self.canarchy=canarchy



        def set_baudrate(self,BAUDRATE):
                rx = self.rx
                tx = self.tx
                if(self.can):
                        self.can.deinit()
                         
                self.can = canio.CAN(rx=rx, tx=tx, baudrate=BAUDRATE, auto_restart=True)
                self.start_listener()
               
        def set_filter(match_list=[]):
                self.matchlist=[]
                for i in match_list:
                        self.matchlist.append(canio.Match(i))

        def start_listener(self,timeout=.2):
                if self.listener:
                        self.listener = None
                self.listener = self.can.listen(matches=self.matchlist, timeout=timeout)

        def receive(self):
                message = self.listener.receive()
                if message:
                        self.canarchy.display.set_active_flag(1,True)
                        try:
                                data = hexlify(message.data).decode("utf-8").upper()
                        except:
                                print("something went wrong with message.data?")
                                return

                        hex_id = hex(message.id)[2:].upper()
                        id = f"{hex_id:0>8}" 
                        
                        if(message.id<=2048):
                                packetinfo = hex_id+"#"+data
                        else:
                                packetinfo = id+"#"+data  



                        self.canarchy.display.add_line("<" + packetinfo)

                        self.outbuffer.append(packetinfo)
                        if(self.display_traffic):
                                print("< ("+str(time.monotonic()) +") " + packetinfo)
                        
                        
                        try:
                                can_callback_onReceive(message)
                        except Exception as e:
                                print ("ERROR in can_callback_onReceive()")
                                print(e)
                

                        if(self.logger):
                                self.logger.log(packetinfo)



        def send(self,id,payload):
                self.canarchy.display.set_active_flag(1,True)

                if(id<=2048):
                        message = canio.Message(id=id, data=payload, extended=False)
                else:
                        message = canio.Message(id=id, data=payload, extended=True)
                self.can.send(message)
                #TODO return soemthing if can works or dont work

                
                try:
                        can_callback_onSend(message)
                except Exception as e:
                        print ("ERROR in can_callback_onSend()")
                        print(e)
                        


        def send_block(self,block):
                try:
                        hex_id,hex_data = block.split('#')
                        id = int(str(hex_id), 16)
                #print(id)
                        data = unhexlify(hex_data)
                        if(self.display_traffic):
                                print("> ("+str(time.monotonic()) +") " + block)

                        self.canarchy.display.add_line(">" + block)
                        self.send(id,data)
                except Exception as e:
                        print("Something went wrong when I tried to send: ", block)
                #TODO return soemthing if can works or dont work

        
        def addLogger(self,logger):
                self.logger=logger



        def work(self):
                self.receive()







