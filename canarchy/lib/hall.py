# Late 2023
# Author overflo
# Part of CANarchy
# Filename: hall.py
# Purpose: Handles the hall effect sensor on Pin 12
# License Details found @ /LICENSE file in this repository



import board
from digitalio import DigitalInOut, Direction, Pull





try:
    from app import hall_callback_onChange
except:
    print("ERROR: hall_callback_onChange not defined in app.py")
    pass



class Hall:
    state = False
    pin = None

    def __init__(self):
        print("Hall.__init__()")
        self.pin = DigitalInOut(board.IO12)
        self.pin.direction = Direction.INPUT
        self.pin.pull = Pull.DOWN


    def work(self):
        if self.state != self.pin.value:
            self.state = self.pin.value
            #print("state changed on hall")
            try:
                hall_callback_onChange(self.state)
            except Exception as e:
                print("ERROR in hall_callback_onChange() ", e)