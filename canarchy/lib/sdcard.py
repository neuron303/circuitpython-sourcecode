# Late 2022
# Author overflo 
# Part of CANarchy
# Filename: sdcard.py
# Purpose: open SD card via SPI, provide hardware layer for logfiles
# License Details found @ /LICENSE file in this repository


import board
import sdcardio
import storage
import os




# is this any useful?!
# try:
#     from app import sd_callback_onClose
# except:
#     print("ERROR: sd_callback_onClose not defined in app.py")
#     pass

# try:
#     from app import sd_callback_onInsert
# except:
#     print("ERROR: sd_callback_onInsert not defined in app.py")
#     pass

# try:
#     from app import sd_callback_onEject
# except:
#     print("ERROR: sd_callback_onEject not defined in app.py")
#     pass



class SDcard:

 
    card=None
    active=False

 

    def __init__(self,SPI,CS):
        print("SDcard.__init__("+str(SPI)+","+str(CS)+")")
        try:
            self.card = sdcardio.SDCard(SPI,CS)
            vfs = storage.VfsFat(self.card)
            storage.mount(vfs, '/sd')
            self.active=True    
            print("SD successfully initialized!")


        except:
            print("SD init FAILED")
            self.active=False




    def close(self):
        self.card.close()
        try:
             sd_callback_onClose()
        except Exception as err:
            print(err)
            pass   


# TODO  pin 14 is connected to card_detect
# implement a read and call callbacks maybe?# no use for this so far.
# sd_callback_onInsert()
# sd_callback_onEject()


