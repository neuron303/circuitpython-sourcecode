# Late 2022
# Author overflo 
# Part of CANarchy
# Filename: serialcom.py
# Purpose:  Serial communication with a human on the other end of the line
# License Details found @ /LICENSE file in this repository




import supervisor
import time
import usb_cdc


class serialcom:

    canarchy = None

    run=False
    connected = False
    active = False

    menu_shown = False
    
    read_raw = False

    await_packet_input = False
    await_logfile_input = False



    menu="\
########################################################################\r\n\
#                                                                      #\r\n\
#  Welcome to CANarchy! Version %s                                    #\r\n\
#  Happy fun times ahead!                                              #\r\n\
#                                                                      #\r\n\
# Do you want to [d]isplay CANbus traffic in this Terminal? Press [D]  #\r\n\
# Do you want to [r]eplay a log file? Press [R]                        #\r\n\
# Do you want to [s]end a packet? Press [S]                            #\r\n\
#                                                                      #\r\n\
########################################################################\r\n\
"

    def __init__(self,canarchy):
        print("serialcom.__init__()")
        self.canarchy = canarchy
        self.start()



    def work(self):

        if not self.run:
            return
        # else:
        #     print("hier")

        if(supervisor.runtime.serial_connected):
            if self.connected == False:
                self.connected=time.monotonic()

            # run the serialcom stuff after 1 second
            if((time.monotonic()- self.connected) >=1 ):
                self.active=True    

        else:
            self.active=False
            self.connected=False

        if(self.active):
            # so we are conncted. we should wait for 1 second before sending the menu so slcand gets a chance to claim the session 
            if self.menu_shown == False:
                self.display_menu()
            self.serial_in()


    def display_menu(self):
        print(self.menu % (self.canarchy.VERSION))
        self.menu_shown=True
        self.read_raw=True
        






    def serial_in(self):
        s=""
        # reads RAW data directly o keypress
        if self.read_raw == True:
            s=""
            available = usb_cdc.console.in_waiting
            while available:
                raw = usb_cdc.console.read(available)
                s = s + raw.decode("utf-8")
                available = usb_cdc.console.in_waiting
        else:
            # reads data once a lineending is detected
            if supervisor.runtime.serial_bytes_available:
                s = input()



        if s:
            value = s.strip()
            if value == "":
                self.display_menu()
                return

            if ( self.await_packet_input ):
                #print("You want to send: ", value)
                self.canarchy.can.send_block(value)
                self.await_packet_input = False



            if ( self.await_logfile_input ):
                try:
                    fnum = int(value)
                except:
                    print("Come on, don't be silly. ", value, " is not a valid number. Try again.")
                    return

                if(fnum<1):
                    print("The files start to count at 1, I know.. unusual. But you are human right?")
                    return

                maxfiles = len(self.canarchy.datalogger.logfiles)    
                if fnum > maxfiles:
                    print(fnum, "is too big a number. Pick one from 1 -", maxfiles)
                    return

                
                self.canarchy.replaylog(self.canarchy,fnum-1)
               



            if(value[0] == 'd' or value[0] == 'D'):
                print("Okay! I will show traffic as it appears on the bus")
                self.canarchy.can.display_traffic = True

            if(value[0] == 's' or value[0] == 'S'):
                print("Please input the packet you like to send in the format <ID>#<DATA> (fE 12345678#DEADBEEF23421337) and press [Enter]")
                self.read_raw = False
                self.await_packet_input = True



            if(value[0] == 'r' or value[0] == 'R'):
                if not len(self.canarchy.datalogger.logfiles) > 0:
                    print("No files are available on the SD card for replay, sorry dear :(")
                    return

                print("Here is a list of files available for replay, my friend.\r\nPick one by entering the number and press [Enter]\r\n\r\nNo.  Name    Size\r")
                counter=1
                for e in self.canarchy.datalogger.logfiles:
                    print("["+str(counter)+"] ",e[0], " ", e[1])
                    counter=counter+1
   
                self.await_logfile_input = True
                self.read_raw = False



            if(value[0] == 'O'):
                # slcan connected!!
                self.stop()
                self.canarchy.slcan.start()

                #self.canarchy.slcan.ack()


    def stop(self):
        #print("serialcom.stop() called")
        self.active=False
        self.read_raw=True
        self.connected=False
        self.run=False

    def start(self):
        #print("serialcom.start() called")
        self.active=False
        self.read_raw=False
        self.connected=False
        self.run=True
        self.menu_shown = False