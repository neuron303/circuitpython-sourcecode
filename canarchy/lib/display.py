# Early 2022
# Author overflo
# Part of canarchy.io
# Filename: display.py
# Purpose: Handles the SSD1606 display on I2C bus
# License Details found @ /LICENSE file in this repository


import time
import busio
import board
import terminalio
import displayio
import adafruit_displayio_ssd1306
displayio.release_displays()

from adafruit_display_text import label
import random
import math



class Display:

    available=False
    canarchy = None
    

    rootcanvas = None

    menubuffer = None
    text_updated = False
    menu_updated = False

    #where we are at the moment
    currentline=0
    maxlines=4
    lines = []

    counter=0



    # set when something happens that should update the display (or something else, that why its here and not in the display driver)
    updatemenu = True
    updatetext = True
    canactive =0
    slcanactive=0
    sdactive=0




    def __init__(self,canarchy):
 

        self.canarchy=canarchy



        self.rootcanvas = displayio.Group()
        self.menubuffer = displayio.Group()
        self.linebuffer1 = displayio.Group()
        self.linebuffer2 = displayio.Group()


        
        self.display_first_buffer = True


        try:

            displayio.release_displays()
            i2c = busio.I2C(scl=board.IO37,sda=board.IO35)
            display_bus = displayio.I2CDisplay(i2c, device_address=0x3c)
            self.oled = adafruit_displayio_ssd1306.SSD1306(display_bus, width=128, height=64)
            self.available = True

            self.show_logo()
            time.sleep(2)

            self.rootcanvas.append(self.menubuffer)
            self.rootcanvas.append(self.linebuffer1)
            self.rootcanvas.append(self.linebuffer2)

        except Exception as e:
            print("Display can not be initalized :( -> ", e)
            self.available=False

        # we might rotate the display with something like this..
        #self.oled.write_cmd(ssd1306.SET_COM_OUT_DIR | 0x01)
        #self.oled.write_cmd(ssd1306.SET_SEG_REMAP | 0x01)





    def set_active_flag(self,flag,on_off):
        
        if(on_off):
            f = math.ceil(time.monotonic())
        else:
            f = 0    

        if(flag==1):
            self.canactive = f

        if(flag==2):
            self.slcanactive = f
        
        if(flag==3):
            self.sdactive = f

        self.updatemenu = True



    def cleanflags(self):
        if(self.canactive>0):
            if(self.canactive+1 <time.monotonic()):
                self.canactive=0
                self.updatemenu =True
       
        if(self.sdactive>0):
            if(self.sdactive+1 <time.monotonic()):
                self.sdactive=0
                self.updatemenu =True







    def add_line(self,text):
        self.updatetext = True
        self.currentline = self.currentline+1
        if(self.currentline>self.maxlines):
            self.currentline=self.maxlines
            self.lines.pop(0)
        self.lines.append(text)
    



    def show_lines(self):

        if not self.updatetext:
            return
        
        self.updatetext  = False

        if (self.display_first_buffer):
            canvas =  self.linebuffer1
        else:
            canvas =  self.linebuffer2   
       
        for i,l in enumerate(self.lines):
            try:
                canvas.pop(0)
            except:
                pass

        for i,l in enumerate(self.lines):
            self.show_text(l,0,(20+i*12),canvas)


        if (self.display_first_buffer):
            self.linebuffer2.hidden=True
            self.linebuffer1.hidden=False
            self.display_first_buffer=False
        else:
            self.linebuffer1.hidden=True
            self.linebuffer2.hidden=False
            self.display_first_buffer=True


        self.show()



    def show_text(self,text,x,y,canvas):
        text_area = label.Label(terminalio.FONT, text=text, color=0xFFFF00, x=x, y=y)
        canvas.append(text_area)





    def show(self):
        if not self.available:
        #    print("DISPLAY NOT AVAILABLE?")
            return 

        #print("display.show()")
        self.oled.show(self.rootcanvas)
        pass




    def show_logo(self):
        if not self.available:
            return

        try:
            bitmap = displayio.OnDiskBitmap("/sd/logo.bmp")
        except:
            bitmap = displayio.OnDiskBitmap("/canarchy/assets/logo.bmp")
        
        # Create a TileGrid to hold the bitmap
        tile_grid = displayio.TileGrid(bitmap, pixel_shader=bitmap.pixel_shader)
        # Create a Group to hold the TileGrid
        cv= displayio.Group()
        # Add the TileGrid to the Group
        cv.append(tile_grid)
        text_area = label.Label(terminalio.FONT, text="v"+self.canarchy.VERSION, color=0xFFFF00, x=100, y=60)
        cv.append(text_area)

        # Add the Group to the Display
        self.oled.show(cv)



    def draw_menu(self):
        if not self.available:
            return
        
        if not self.updatemenu:
            return
        
        self.updatemenu = False

        can =   1 if (self.canactive > 0  ) else 0
        sys = 1 if (self.slcanactive > 0) else 0
        sd =   1 if (self.sdactive > 0   ) else 0

        bitmap = displayio.OnDiskBitmap("/canarchy/assets/m_"+str(sys)+"_"+str(can)+"_"+str(sd)+".bmp")
    

        # Create a TileGrid to hold the bitmap
        tile_grid = displayio.TileGrid(bitmap, pixel_shader=bitmap.pixel_shader)

        try:
            self.menubuffer.pop(0)
        except:
            pass

        self.menubuffer.append(tile_grid)
    
        

    

    def work(self):
        self.cleanflags()
        self.draw_menu()
        #self.add_line(str(self.counter))
        self.show_lines()
        #self.counter=self.counter+1