# Late 2022
# Author overflo 
# Part of CANarchy
# Filename: datalogger.py
# Purpose: Datalogging on the SD card
# License Details found @ /LICENSE file in this repository


import os
import time
import busio
import board



class DataLogger:
    sd = None
    fd = None
    active= False

    logfiles=[]

    canarchy= None

    def __init__(self,canarchy):
        print("Datalogger.__init__()")

        self.canarchy=canarchy

        from . import sdcard
        try:
            self.sd = sdcard.SDcard(busio.SPI(board.SCK,board.MOSI,board.MISO),board.IO5)
            self.createLogfileList()

        except Exception as e:
            print("Something failed for SD init " + str(e))



    def getFilelist(self):
        files  = os.listdir('/sd')
        #print(files)
        return files


    def open(self,filename):
        if(self.fd):
            self.close()
        self.fd = open("/sd/"+filename, 'w')
        print("opened /sd/" + filename + " (w)")

 
    def read(self):
        self.fd.seek(0)
        lines = self.fd.readlines()
        print(lines)




    def log(self,content):
        #print("log() called")
        data="(" + str(time.monotonic())+") " + content + "\r\n"
        #data=content + "\r\n"
        self.write(data)


    def write(self,data):



        if not self.fd:
            return
        
        self.canarchy.display.set_active_flag(3,True)

        try:
            self.fd.write(data)
            self.flush()
        except:
            print("LOG WRITE FAILED")

    def flush(self):
        self.fd.flush()


    def close(self):
        print("closing current fd")
        self.fd.close()
        self.fd=None


    def createLogfileList(self):
        try:
            files = self.getFilelist()
            print(files)
            for f in files:
                if ".log" in f:
                    f_size = os.stat("/sd/" + f)
                    entry=[f,f_size[6]]
                    self.logfiles.append(entry)


        except:
            print("Something went wrong when creating the file list")

    def createNewLogfile(self):
        try:
            files = self.getFilelist()
            #print(files)
            current_max_id=0
            for f in files:
                if ".log" in f:
                    id,rest = f.split(".")
                    if(int(id) > current_max_id):
                        current_max_id=int(id)

            newlog=str(current_max_id+1)+".log"
            print("Creating new logfile: "+newlog)
            self.open(newlog)      
        except:
            print("Something went wrong when creating the new file")






